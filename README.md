# Video_lumberjack by Joel Rodiel-Lucero 2019

Edit lines 8-10 to change the Input, Slice Duration, and Output.

Add 'lumberjack.sh' into /etc/cron.hourly/

Process:
	1. Checks if 'Input' dir is empty. Quits program if it is.
	2. Gets all files in 'Input' dir and checks if any of them are video files. If no video files quits program.
	3. Pick the topmost video file and gets its length in seconds.
	4. Divides the length by desired 'slice duration'.
	5. Checks if 'Output' dir exists, if not it creates one.
	6. Creates a directory in the 'Output' dir with the name of the file.
	7. Uses ffmpeg to slice video and puts them in directory in 'Output' dir.
	8. Moves original video file in 'Input' dir into directory in 'Output' dir.
	9. Deletes original video file in 'Input' dir.
