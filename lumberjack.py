import sys
import os
from os import walk
import math
import time

#################################################
inboxDir = 'input'   # Change this to desired dir
sliceDuration = 10   # Slice duration in seconds
outputDir = 'output' # Change this to desired dir
#################################################

# If Inbox dir empty, quit
if not os.listdir(inboxDir) :
    print("Inbox dir empty, ending process...")
    sys.exit()

files = []

# Get all files in Inbox dir and store in files array
for (dirPath, dirName, fileNames) in walk(inboxDir) :
    files.extend(fileNames)

extensionCheck = False
extensionList = ['.mp4'] # List of extensions
fileToEdit = ""

# Check if files match correct extensions (ex: .mp4)
for v in files :
    for ext in extensionList :
        if v.endswith(ext) :
            extensionCheck = True
            fileToEdit = v
            print("File", v, "is a", ext, "!! Cool!")

if not extensionCheck :
    print("No video files in inbox, ending proces...")
    sys.exit()

nameOfFile = fileToEdit[0:-4]

durationCmd = "ffprobe -i " + inboxDir + "/" + fileToEdit + " -show_format | grep duration"

# Execute command to get file duration
videoDuration = os.popen(durationCmd).read()

# Slice cmd output and convert to float
videoDuration = float(videoDuration[9:-1])

# Calculate how many parts to slice
videoParts = int(math.ceil(videoDuration / sliceDuration))

# If Output dir doesn't exist create it
if not os.path.isdir(outputDir) :
    os.system("mkdir " + outputDir)

# Create slices folder in Output dir
os.system("mkdir " + outputDir + "/" + nameOfFile)

outputDir = outputDir + "/" + nameOfFile

# Slice the videos!
for dur in range(videoParts) :
    os.system("ffmpeg -i " + inboxDir + "/" + fileToEdit + " -ss "+ str(time.strftime('%H:%M:%S', time.gmtime(sliceDuration*dur))) + " -t " + str(time.strftime('%H:%M:%S', time.gmtime(sliceDuration))) + " -async 1 " + outputDir + "/" + nameOfFile + "_part"+ str(dur+1) + ".mp4")

# Move the original video to output dir
moveCmd = "cp " + inboxDir + "/" + fileToEdit + " " + outputDir + "/" + fileToEdit
os.system(moveCmd)

# Delete original video so it doesnt call it again!
deleteCmd = "rm " + inboxDir + "/" + fileToEdit
os.system(deleteCmd)

# Clear the ugly console
os.system("clear")

# End process
sys.exit()
